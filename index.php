<DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Berlatih PHP</title>
    </head>
    <body>
        <?php 
            $panjang = 10; // contoh assignment
            $lebar = 8; // contoh assignment
            $luas = $panjang * $lebar; // contoh assignment juga
            echo $luas;
            echo "<br>";
            echo "<br>";
            // tanda '=' mewakili proses assignment,
            // ada beberapa tanda lagi yang dapat
            // melakukan assignment,
            // assignment penjumlahan diwakili tanda '+=' contohnya
            $tambahdua = 0;
            $tambahdua += 2;
            echo $tambahdua."<br>" ;
            echo "<br>";
            // hasilnya 2
            // hal ini sama dengan pernyatan :
            // $tambahdua = $tambahdua + 2;
            // assignment pengurangan diwakili tanda '-=' contohnya
            $clip = 22;
            $clip -= 2; // sama dengan $clip = $clip - 2;
            echo $clip;
            echo "<br>";
            echo "<br>";

            $angka = 10;
            echo $angka == 10; // true atau 1
            echo "<br>";
            echo "<br>";

            var_dump($angka == 9); // bool(false) atau 0
            echo "<br>";
            echo "<br>";

            $sifat= "rajin";
            var_dump($sifat != "malas"); // bool(true)
            echo "<br>";
            echo "<br>";

            echo $sifat <> "bandel"; // 1
            echo "<br>";
            echo "<br>";

            $angka = 10;
            var_dump($angka === "10");// bool(false)
            echo "<br>";
            echo "<br>";

            var_dump($angka === 10);// bool(true)
            echo "<br>";
            echo "<br>";

            $angka = 17;
            var_dump($angka < 20); // bool(true)
            echo "<br>";
            echo "<br>";
            var_dump($angka <= 17); // bool(true)
            echo "<br>";
            echo "<br>";
            var_dump($angka >= 15); // bool(true)
            echo "<br>";
            echo "<br>";
            var_dump($angka > 12); // bool(true)
            echo "<br>";
            echo "<br>";

            var_dump(true && true); // bool(true)
            var_dump(true && false);// bool(false)
            var_dump(false && true);// bool(false)
            var_dump(false && false);// bool(false)
            echo "<br>";
            echo "<br>";

            var_dump(true || true); // bool(true)
            var_dump(true || false); // bool(true)
            var_dump(false || true); // bool(true)
            var_dump(false || false); // bool(false)
            echo "<br>";
            echo "<br>";
            
            $hari_ini = "senin";
            if($hari_ini == "senin") {
            echo "I Love Monday";
            } else {
            echo "Ini bukan hari senin";
            };
            echo "<br>";
            echo "<br>";

            $materi= "PHP";
            echo ($materi == "PHP")?"Hari ini belajar PHP": "Hari ini bukan materi PHP";
            echo "<br>";
            echo "<br>";

            function kenalkan($nama) {
                echo "kenalkan nama saya $nama";
              }
              
              // panggil function kenalkan
             kenalkan("abduh");
            echo "<br>";
            echo "<br>";

            function buat_nama_kapital($nama) {
                return ucwords($nama);
            }
              
            $nama_lengkap = buat_nama_kapital("muhamad abduh");
            echo $nama_lengkap; // "Muhamad Abduh"
            echo "<br>";
            echo "<br>";
            
            
            // memanggil function di dalam function lain
            function perkenalan($nama, $asal) {
                echo "perkenalkan nama saya ". buat_nama_kapital($nama). " asal saya dari $asal";
            }
              
            perkenalan("muhamad abduh", "bandung");
            echo "<br>";
            echo "<br>";

            for($i = 1; $i < 5 ; $i++ ){
                echo "ini adalah looping ke $i <br>";
            }
            echo "<br>";
            echo "<br>";

            $x = 1;

            while($x <= 5) {
            echo "$x - satu Sampai lima<br>";
            $x++;
            } 
            echo "<br>";
            echo "<br>";

            $animals = array("cat", "dog", "snake", "ant", "lion"); 

            foreach ($animals as $animal) {
            echo "$animal <br>";
            }
            echo "<br>";
            echo "<br>";

            $age = array("Rezky"=>"25", "Abduh"=>"29", "Iqbal"=>"33");

            foreach($age as $x => $val) {
            echo "$x = $val<br>";
            }
        ?>
    </body>
</html>